
import React from "react";
import { Route, Switch } from "react-router-dom";
// reactstrap components
import { Link } from 'react-router-dom';
import queryString from "query-string";
import "./../assets/css/iframe.scss";
// core components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Modal,
  Row,
  Col
} from "reactstrap";
import StarRatingComponent from 'react-star-rating-component';
import routes from "routes.js";

class Iframe extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect_url: '',
      doctor_data:{
        first_name: "KARL",
        last_name: "HSIEH",
        credential: "M.D.",
        datelabel: "",
        date: "",
        time: "",
        appointment_id: 0,
        provider_id: 0,
        address1:'289 IRELAND AVE',
        address2:'BUILDING 851',
        city:'KNOX',
        state:'FORT',
        zip:'40121',
        distance:0,
        taxonomy_code:'',
        appointments:[],
        reviews: {total:0},
        rating: {overall:4.5}
      }
    }
  }
    

  componentDidMount() {
    
    let params = queryString.parse(this.props.location.search);
    let providerId = params.p;
    var uuid = require("uuid");
    var valu31 = uuid.v4();
    let make_redirect_url = "http://testyourapp.online/doctorapplatest1ankit/#/info/"+valu31+"-"+providerId;
    this.setState({
        redirect_url: make_redirect_url
    });
    
    console.log(this.state.redirect_url);
    

    let url = "https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/provider/"+providerId+"?=detail";

    const requestOptions = {
    method: "GET",
    headers: { "Content-Type": "application/json", 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' },
    };
    return (
    fetch(`${url}`, requestOptions)
    .then(res => {
      res.json().then(json => {
        
        let provider_details_ob = json.result;
        if(provider_details_ob){
          this.setState({
              doctor_data: provider_details_ob
          });
        }
        });
      })
    );
    document.body.classList.add("bg-default");

    

  }
  componentWillUnmount() {
    document.body.classList.remove("bg-default");
  }

  chartmain(appointment_id){
      
      var uuid = require("uuid");
      var valu31 = uuid.v4();
      var valu32 = uuid.v4();
      let providerId = this.state.doctor_data.id;
      let make_redirect_url = "http://testyourapp.online/doctorapplatest1ankit/#/appointments/"+valu31+"-"+providerId+"/"+valu32+"-"+appointment_id;
      window.open(make_redirect_url, "_blank");
  }

  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/auth") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  render() {
    return (
      <>
        <Col col="12" xl="12" md="12" sm="12" xs="12" className="c-grid list-grid">
              
          <div className="doctor-block-wrap list-view-wrap">
              <div className="row align-items-center">
                <Col col="12" xl="4" md="4" sm="4" xs="4" className="">    
                  <div className="doctor-block">
                  <Link to="#" className="form-links">
                    <img onClick={()=> window.open(this.state.redirect_url, "_blank")} className="img-fluid" src={require('./../assets/img/doctors/dr1.jpg')}  alt="fireSpot"/>
                    </Link>
                    
                  </div>
                  </Col>
                  
                  <Col col="12" xl="8" md="8" sm="8" xs="8" className="">
                  <div className="row align-items-center">
                  <Col col="12" xl="6" md="6" sm="6" xs="12" className="">
                    
                  <div className="list-view-doctor-info">
                      <h3 onClick={()=> window.open(this.state.redirect_url, "_blank")}>{this.state.doctor_data.first_name} {this.state.doctor_data.last_name}, {this.state.doctor_data.credential}</h3>
                      <span>{this.state.doctor_data.taxonomy_name}</span>
                      <address>{this.state.doctor_data.address1}, {this.state.doctor_data.address2}, {this.state.doctor_data.city} {this.state.doctor_data.zip}</address>
                      <p>0{this.state.doctor_data.distance} Miles</p>
                      </div>
                    </Col>
                    <Col col="12" xl="6" md="6" sm="6" xs="12" className="">

                    <div className="doctor-info list-info">
                  
                      <div className="ratings">
                        <div className="rating-inner">
                          <div className="star-ratings">
                             <StarRatingComponent 
                            name="rate2" editing={false}
                            starCount={this.state.doctor_data.rating.overall} 
                            starColor="#f28a18" 
                            emptyStarColor="#000"
                            value={this.state.doctor_data.rating.overall}
                            
                          />
                          </div>

                        </div>
                        <div className="ratings">
                        <div className="reviews">
                          <p>{this.state.doctor_data.reviews.total} reviews</p>
                        </div>
                        </div>
                      </div>
                      <div className="doctor-bottom">
                        <div className="buttons-grp">
                        <p>Today</p> 

                    {this.state.doctor_data.appointments.slice(0, 3).map((appointment_times, idx_at) => {
                    return ( 
                    <div className="single-btn" key={idx_at}>                           
                      <a onClick={() => { this.chartmain(appointment_times.appointment_id);}} className={idx_at===0 ? 'active' : ''} role="button" aria-pressed="true" >{appointment_times.appointment_time}</a>
                            
                        </div>            
                            )
                      })}
                        </div>
                      </div>
                  </div>
                    </Col>

                  </div>    
                  
                  </Col>
              </div>
              
          </div>
          <div className="poweredBy">
           Powered by Doktorup.com  
          </div>  
        </Col>
      </>
    );
  }
}

export default Iframe;
