import { Alert } from "react-bootstrap";
import React, { useState } from "react";

export function AlertComponent() {
    const [ show, setShow ] = useState(true);

    setTimeout(() => {
        setShow(false)
    }, 3000)

    if (show) {
        return (
            <Alert variant="success">
                <Alert.Heading>Success</Alert.Heading>
                <p>
                    Added in favourite list
                </p>
            </Alert>
        );
    } else return <></>
}