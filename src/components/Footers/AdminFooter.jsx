
import React from "react";

// reactstrap components
import { Container, Row, Col, Nav, NavItem, NavLink } from "reactstrap";

class Footer extends React.Component {
  render() {
    return (
      
      <footer className="footer">
        <Row className="align-items-center justify-content-xl-between">
          <Col xl="12">
            <div className="copyright text-xl-left">
            <a
                className=" ml-1 footer-cmpny-link"
                href=""
                rel=""
                target="_blank"
              >
                Copyright Doktorup
              </a> 
             ©2019{" "}
              
            </div>
          </Col>

          
        </Row>
      </footer>
    );
  }
}

export default Footer;
