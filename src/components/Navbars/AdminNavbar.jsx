
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  DropdownToggle,
  Form,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  InputGroup,
  Navbar,
  Nav,
  Container,
  Media
} from "reactstrap";

class AdminNavbar extends React.Component {
  logout(){
    localStorage.removeItem("gmailuser");
    // localStorage.removeItem("favcard");

  }
  render() {
    let gmailuser = localStorage.getItem("gmailuser");
    let profile = localStorage.getItem("profile");
    return (
      <>
        <Navbar className="navbar-top navbar-dark" expand="md" id="navbar-main">
          <Container fluid>
            {/* <Link
              className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block"
              to="/"
            >
              {this.props.brandText}
            </Link> */}
            <Form className="navbar-search navbar-search-dark form-inline mr-3 d-none d-md-flex srch-from">
              <FormGroup className="mb-0 form-inner">
                <InputGroup className="input-group-alternative">
                  
                  <Input placeholder="Time, illness, speciality, doctor name…" type="text" />
                  <InputGroupAddon addonType="prepend">
                    {/* <InputGroupText>
                      <i className="fas fa-search" />
                    </InputGroupText> */}
                    <button type="submit" className="btn srch-btn">Search</button>
                  </InputGroupAddon>
                </InputGroup>
              </FormGroup>
            </Form>
            {gmailuser == null ? (
                    <span></span>
                  ) : (
                    <Nav className="align-items-center d-none d-md-flex" navbar>
                    <UncontrolledDropdown nav>
                      <DropdownToggle className="pr-0" nav>
                        <Media className="align-items-center">
                        <Media className="ml-2 d-lg-block ntf-bell-block">
                            <span className="mb-0 text-sm font-weight-bold ntf-bell">
                            <svg id="alert_icon" data-name="alert icon" xmlns="http://www.w3.org/2000/svg" width="19.175" height="24" viewBox="0 0 19.175 24">
        <path id="Path_359" data-name="Path 359" d="M19.537,28.336a.777.777,0,0,0-.762.612,1.5,1.5,0,0,1-.3.654,1.134,1.134,0,0,1-.966.354,1.153,1.153,0,0,1-.966-.354,1.5,1.5,0,0,1-.3-.654.777.777,0,0,0-.762-.612h0a.782.782,0,0,0-.762.954,2.68,2.68,0,0,0,2.789,2.225A2.675,2.675,0,0,0,20.3,29.29a.785.785,0,0,0-.762-.954Z" transform="translate(-7.939 -7.515)" fill="#7b7a7a"/>
        <path id="Path_360" data-name="Path 360" d="M25.709,21.7c-.924-1.218-2.741-1.932-2.741-7.384,0-5.6-2.471-7.846-4.775-8.386-.216-.054-.372-.126-.372-.354V5.406a1.471,1.471,0,0,0-1.44-1.476h-.036a1.471,1.471,0,0,0-1.44,1.476V5.58c0,.222-.156.3-.372.354-2.309.546-4.775,2.789-4.775,8.386,0,5.453-1.818,6.16-2.741,7.384a1.191,1.191,0,0,0,.954,1.908h16.8A1.192,1.192,0,0,0,25.709,21.7Zm-2.339.348H9.381a.263.263,0,0,1-.2-.438,7.267,7.267,0,0,0,1.26-2A13.594,13.594,0,0,0,11.3,14.32a9.2,9.2,0,0,1,1.254-5.207,3.849,3.849,0,0,1,2.321-1.656,2.1,2.1,0,0,0,1.116-.63.474.474,0,0,1,.714-.012,2.173,2.173,0,0,0,1.128.642,3.849,3.849,0,0,1,2.321,1.656,9.2,9.2,0,0,1,1.254,5.207,13.594,13.594,0,0,0,.858,5.291,7.35,7.35,0,0,0,1.29,2.033A.248.248,0,0,1,23.369,22.052Z" transform="translate(-6.775 -3.93)" fill="#7b7a7a"/>
      </svg>
      
                            </span>
                          </Media>
                        <Media className="ml-2 d-none d-lg-block">
                            <span className="mb-0 text-sm font-weight-bold usr-name">
                            {gmailuser}
                            </span>
                          </Media>
                          <span className="avatar avatar-sm rounded-circle">
                            <img
                              alt="..."
                              src={require("assets/img/theme/ss.png")}
                            />
                          </span>
                          
                        </Media>
                      </DropdownToggle>
                      <DropdownMenu className="dropdown-menu-arrow" right>
                        <DropdownItem className="noti-title" header tag="div">
                          <h6 className="text-overflow m-0">Welcome!</h6>
                        </DropdownItem>
                        <DropdownItem to="/admin/user-profile" tag={Link}>
                          <i className="ni ni-single-02" />
                          <span>My profile</span>
                          
                        </DropdownItem>
                        <DropdownItem to="/admin/user-profile" tag={Link}>
                          <i className="ni ni-settings-gear-65" />
                          <span>Settings</span>
                        </DropdownItem>
                        <DropdownItem to="/admin/user-profile" tag={Link}>
                          <i className="ni ni-calendar-grid-58" />
                          <span>Activity</span>
                        </DropdownItem>
                        <DropdownItem to="/admin/user-profile" tag={Link}>
                          <i className="ni ni-support-16" />
                          <span>Support</span>
                        </DropdownItem>
                        <DropdownItem divider />
                        <DropdownItem href="#pablo" onClick={e => e.preventDefault()}>
                          <i className="ni ni-user-run" />
                          <span  onClick={() => { this.logout();}}><Link to="/" className="logout-text">
                  Logout 
                </Link>
</span>
                        </DropdownItem>
                      </DropdownMenu>
                    </UncontrolledDropdown>
                  </Nav>
               
                  )}
             </Container>
        </Navbar>
      </>
    );
  }
}

export default AdminNavbar;
