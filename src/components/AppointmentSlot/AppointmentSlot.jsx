import React, { useRef } from "react";

export default function AppointmentSlot(props) {
    const slotButton = useRef();
    const classUpdator = (event, index) => {
        console.log(index);
        event.preventDefault()
        slotButton.current.classList.contains("active") ? slotButton.current.classList.remove("active") : slotButton.current.classList.add("active")
    }

    return (
        <>
            <li>
                <a href="#" class="" ref={slotButton} onClick={(e) => classUpdator(e, props.i)} role="button" aria-pressed="true" key={props.i}>{props.time}</a>
            </li>
        </>
    )
}