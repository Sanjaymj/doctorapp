
import React  from "react";
// node.js library that concatenates classes (strings)
import { Route, Switch } from "react-router-dom";
import classnames from "classnames";
// javascipt plugin for creating charts
import Chart from "chart.js";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";
import { Link } from 'react-router-dom';
import { AlertComponent } from './../components/Alert/Alert';
import AdminNavbar from "./../components/Navbars/AdminNavbar.jsx";
import GoogleLogin from 'react-google-login';
import axios from 'axios';

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Modal,
  Row,
  Col
} from "reactstrap";
import routes from "routes.js";

// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "variables/charts.jsx";

import Header from "components/Headers/Header.jsx";
import StarRatingComponent from 'react-star-rating-component';

import $ from 'jquery';
import { isConstructorDeclaration } from "typescript";
class Index extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      favcard: 0,
      valuran:'',
    gmailuser:'',
    persons: [],
    nearby_list: [],
    doctor_list: [],
    session_id:'',
    isAuthenticated: false,
    user: null, 
    apires:'',
    token: '',
    Username: "",
    Email: "",
    Password: "",
    Firstname: "",
    Lastname: "",
    Validation: "",
    Usernamelogin: "",
    Passwordlogin: "",
    Validationlogin: "",
    ForgetValidation: "",
    Useremail: "",
    Message: "",
    isloaded1: true,
    activeNav: 1,
    chartExample1Data: "data1",
    exampleModal: false,
    activeDoc:{
      Firstname: "",
      Lastname: "",
      datelabel: "",
      date: "",
      time: "",
      appointment_id: 0,
      provider_id: 0,
      address1:'',
      address2:'',
      city:'',
      state:'',
      zip:'',
      distance:'',
      taxonomy_code:''
    }
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitlogin = this.onSubmitlogin.bind(this);
    // this.onForgetSubmit = this.onForgetSubmit.bind(this);
    }
    logout = () => {
      this.setState({isAuthenticated: false, token: '', session_id:'', user: null})
  };
  googleResponse = (e) => {};
  onFailure = (error) => {
    alert(error);
  }


   googleResponse = (response) => {
      const tokenBlob = new Blob([JSON.stringify({access_token: response.accessToken}, null, 2)], {type : 'application/json'});
      const options = {
          method: 'POST',
          body: tokenBlob,
          mode: 'cors',
          cache: 'default'
      };
      console.log("tokenBlob",tokenBlob);
      fetch('http://http://testyourapp.online/api/v1/auth/google', options).then(r => {
          const token = r.headers.get('x-auth-token');
          console.log("Google response",r.json);
          r.json().then(user => {
              if (token) {
                  this.setState({isAuthenticated: true, user, token})
              }
          });
      })
  };
    onSubmit(e) {
      console.log("Insie");
      let user_name = this.state.Username;
      let email = this.state.Email;
      let password = this.state.Password;
      let first_name = this.state.Firstname;
      let last_name = this.state.Lastname;
            e.preventDefault();
    
    
      // let environement = ENVIORMENT.devlopmenturl;
      // console.log(environement);
      let url = "https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/user";
      const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json", 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' },
      body: JSON.stringify({ user_name,email, password, first_name, last_name })
      };
      return (
      fetch(`${url}`, requestOptions)
      // .then(res => res.json())
      .then(res => {
        console.log(res);
      if (res.status != 200) {
        res.json().then(json => {
          console.log(json);
          this.setState({
            Validation: json.message
            });
          });
     
      } else {
      res.json().then(json => {
      console.log(json);
      this.setState({
        Validation: json.message
        });
      });
      }
      //store user details and jwt token in local storage to keep user logged in between page refreshes
      // localStorage.setItem("user", JSON.stringify(user.status));
      // localStorage.setItem("username", JSON.stringify(this.state.Username));
      return email;
      })
      );
      }

      onSubmitlogin(e) {
        
        let email = this.state.Usernamelogin;
        let password = this.state.Passwordlogin;
        e.preventDefault();
      
      
        // let environement = ENVIORMENT.devlopmenturl;
        // console.log(environement);
        let url = "https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/login";
        const requestOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json", 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' },
        body: JSON.stringify({ email, password })
        };
        return (
        fetch(`${url}`, requestOptions)
        // .then(res => res.json())
        .then(res => {
          
          res.json().then(json => {
            console.log("login response",json ,json.result.session_id);
            if(json.success == true){
        this.setState({
            Validationlogin: ""
          });
         this.setState({
            session_id:json.result.session_id
          });
          this.setState({ 
            favcard : 1
            });
          localStorage.setItem("gmailuser", json.result.email);
          localStorage.setItem("userID", json.result.id);
          localStorage.setItem("session_id", json.result.session_id);
          localStorage.setItem("favcard", "card");
        if(this.state.favcard == 1){
          this.favmain(this.state.doctor_list.nearby.providers.filter((i) => i.id === this.state.activeDoc.provider_id)[0])
          // this.getDashboardData()
          this.props.history.push("/doctor_list");
        } else {
        this.rops.history.push({pathname: "/admin/appointments",search: `?provider_id=${this.state.activeDoc.provider_id}&appointment_id=${this.state.activeDoc.appointment_id}`,state: this.state.activeDoc });
        }
            }
            else{
              console.log(json.error.message);
              this.setState({
                Validationlogin: json.error.message
              });
            }
            
            
           
            });
        return email;
        })
        );
        }

  toggleModal = state => {
    
    this.setState({
      [state]: !this.state[state]
    });
  };

  
  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1"
    });
    let wow = () => {
      console.log(this.state);
    };
    wow.bind(this);
    setTimeout(() => wow(), 1000);
    // this.chartReference.update();
  };
  componentDidMount(){
        
    this.setState({
      Validation: ""
      });
    

    console.log("token",this.state.token, this.state.session_id);
     //https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/provider
       


       // api call for dahsbord doctor list
       this.getDashboardData()
  }

  getCompareData(){
    let session_id = localStorage.getItem("session_id");
    let user_id = localStorage.getItem("userID");
    axios.get(`https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/user/${user_id}?display=all`, {
      headers: {
        'Authorization': session_id,
        'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM',
      }
    })
      .then(res => {
        console.log("data to update: ", this.state.doctor_list.nearby.providers);
        console.log("compare data: ", res.data.result.favourites);
        this.state.doctor_list.nearby.providers.forEach((elem, indx)=>{
          if (res.data.result.favourites.some((i)=> i == elem.id )) {
            let a = this.state.doctor_list
              a.nearby.providers[indx].favourite = true
              this.setState({
                doctor_data : a
              })
          }
        })
      })
  }

  getDashboardData(){
    let email = localStorage.getItem("gmailuser");
    let url = "https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/find/dashboard?zipcode=94122";
        const requestOptions = {
        method: "GET",
        headers: { "Content-Type": "application/json", 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' },
        };
         return (
        fetch(`${url}`, requestOptions)
        .then(res => {
          res.json().then(json => {
            email && this.getCompareData()
            let dashbord_ob = json.result;
            this.setState({
              doctor_list: dashbord_ob
              });             
              console.log(" doctor_list",this.state.doctor_list);
              console.log(" appointments",this.state.doctor_list.recommend2.providers);
            });
        })
        );
  }

  appointmentPopup() {
    $(document).ready(function(){
  
      $("#1").hide();
      $("#2").hide();
      $("#3").hide();
      $("#4").hide();$("#login").hide();
      $("#5").hide();
      $("#6").hide();
      $("#msbmt").hide();
      $("#spin-loader").hide();
      $("#spin-popup").show();
      $("#mcncl").hide();
      console.log("isnide");
    });
  }
  chartmain1(doctor_data, appointments, datelabel){
    // let valu31 = Math.random().toString(36).substring(32);
    
    var uuid = require("uuid");
    var valu31 = uuid.v4();
    var providerId = valu31+"-"+this.state.activeDoc.provider_id;
    // let valu31 = Math.random().toString(36).slice(-50);
    this.setState({
      activeDoc: {
        valuran: Math.random().toString(36).substring(32),
        Firstname: doctor_data.first_name,
        address1: doctor_data.address1,
        address2: doctor_data.address2,
        city: doctor_data.city,
        state: doctor_data.state,
        zip: doctor_data.zip,
        distance: doctor_data.distance,
        taxonomy_code: doctor_data.taxonomy_name,
        Lastname: doctor_data.last_name,
        datelabel: datelabel,
        date: appointments.appointment_date,
        time: appointments.appointment_time,
        provider_id: doctor_data.id,
        appointment_id: appointments.appointment_id,
      }
      });
      console.log(this.state.activeDoc);
      if(this.state.activeDoc.provider_id != 0){
        //this.props.history.push({pathname: `/info/${valu31}`,state: this.state.activeDoc });
        this.props.history.push({pathname: `/info/${providerId}`,state: this.state.activeDoc });
        // this.props.history.push({pathname: "/admin/doctor_profile",search: `?info=${this.state.activeDoc.provider_id}`,state: this.state.activeDoc });        
      }
     // this.props.history.push({pathname: "/admin/doctor_profile",search: `?provider_id=${this.state.activeDoc.provider_id}&appointment_id=${this.state.activeDoc.appointment_id}`,state: this.state.activeDoc });
  //     let gmailuser1 = localStorage.getItem("gmailuser");
  //     console.log(gmailuser1);
  //     if(gmailuser1 == null){
  //       console.log("");
  //   $(document).ready(function(){show()
  
  //     $("#1").show();
  //     $("#2").show();
  //     $("#3").hide();
  //     $("#4").hide();$("#login").hide();
  //     $("#msbmt").hide();
  //     $("#spin-loader").hide();
  //     $("#spin-popup").hide();
  //     console.log("isnide");
  //   });
  // }
  // else{
  //   this.props.history.push({pathname: "/admin/appointments",search: `?provider_id=${this.state.activeDoc.provider_id}&appointment_id=${this.state.activeDoc.appointment_id}`,state: this.state.activeDoc });
  // }
  }
  favmain(doctor_data){
    let gmailuser1 = localStorage.getItem("gmailuser");
    console.log(gmailuser1);
    console.log(doctor_data);
    this.setState({
      activeDoc: {
        Firstname: doctor_data.first_name,
        address1: doctor_data.address1,
        address2: doctor_data.address2,
        city: doctor_data.city,
        state: doctor_data.state,
        zip: doctor_data.zip,
        distance: doctor_data.distance,
        taxonomy_code: doctor_data.taxonomy_name,
        Lastname: doctor_data.last_name,
        provider_id: doctor_data.id,
      }
      });

       let session_id = localStorage.getItem("session_id");
      console.log("session_id",session_id);
      console.log(gmailuser1);
       if(gmailuser1 == null){
        console.log("not login");
        
        $(document).ready(function(){
          $("#1").show();
          $("#2").show();
          $("#3").hide();
          $("#4").hide();$("#login").hide();
           $("#5").hide();
            $("#6").hide();
          $("#msbmt").hide();
          $("#spin-loader").hide();
          $("#spin-popup").hide();
          console.log("isnide");
        });
       }
       else{
 
        let url = `https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/provider/favourite/${doctor_data.id}`;
        const requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json", 
        'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' ,
        'Authorization': `Bearer ${session_id}`,
        },

        };
        return (
        fetch(`${url}`, requestOptions)
        // .then(res => res.json())
        .then(res => {
          
          res.json().then(json => {
            console.log("fav resp: ",json);
            if(json.success == false){
                $("#exist").show();
                $("#remove").hide();
                $("#add").hide();
                setTimeout(()=>{
                  $("#add").hide();
                  $("#exist").hide();
                  $("#remove").hide();
                },3000)
                console.log("It's already favourited");
            } else {
              let a = this.state.doctor_list
              a.nearby.providers.filter((i)=> i.id === doctor_data.id)[0].favourite = true
              this.setState({
                doctor_data : a
              },()=> {
                $("#add").show();
                $("#remove").hide();
                $("#exist").hide();
                setTimeout(()=>{
                  $("#add").hide();
                  $("#exist").hide();
                  $("#remove").hide();
                },3000)
                console.log(" Added in favourite list");
              })
              // $("#5").show();
            }
            });
        })
        );
       }
  }

  un_favmain(doctor_data){
    let gmailuser1 = localStorage.getItem("gmailuser");
    console.log(gmailuser1);
    console.log(doctor_data.id);
    this.setState({
      activeDoc: {
        Firstname: doctor_data.first_name,
        address1: doctor_data.address1,
        address2: doctor_data.address2,
        city: doctor_data.city,
        state: doctor_data.state,
        zip: doctor_data.zip,
        distance: doctor_data.distance,
        taxonomy_code: doctor_data.taxonomy_name,
        Lastname: doctor_data.last_name,
        provider_id: doctor_data.id,
      }
      });

      console.log(gmailuser1);
       if(gmailuser1 == null){
        console.log("not login");
        
        $(document).ready(function(){
          $("#1").show();
          $("#2").show();
          $("#3").hide();
          $("#4").hide();$("#login").hide();
          $("#msbmt").hide();
          $("#spin-loader").hide();
          $("#spin-popup").hide();
          console.log("isnide");
        });
       }
       else{
        $(document).ready(function(){
  
          $("#1").hide();
          $("#2").hide();
          $("#3").hide();
          $("#4").hide();$("#login").hide();
          $("#msbmt").hide();
          $("#spin-loader").hide();
          $("#spin-popup").hide();
          console.log("isnide");
        });
        let session_id = localStorage.getItem("session_id");
        console.log(session_id);
       
        let url = `https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/provider/favourite/${doctor_data.id}?action=DELETE`;
        const requestOptions = {
        method: "PUT",
        headers: { "Content-Type": "application/json", 
        'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' ,
        'Authorization': `Bearer ${session_id}`,
        },

        };
        return (
        fetch(`${url}`, requestOptions)
        // .then(res => res.json())
        .then(res => {
          res.json().then(json => {
            console.log("unfav: ",json);
            if(json.success == false){
              alert(json.error.message);
            }
            else{
              let a = this.state.doctor_list
              a.nearby.providers.filter((i)=> i.id === doctor_data.id)[0].favourite = false
              this.setState({
                doctor_data : a
              },()=> {
                $("#remove").show();
                $("#exist").hide();
                $("#add").hide();
                setTimeout(()=>{
                  $("#add").hide();
                  $("#exist").hide();
                  $("#remove").hide();
                },3000)
                console.log("Removed from favourite list");
            })
            }
          });
        })
        );
       }
  }
  chartmain(doctor_data, appointment_times, datelabel){
    this.setState({
      activeDoc: {
        Firstname: doctor_data.first_name,
        address1: doctor_data.address1,
        address2: doctor_data.address2,
        city: doctor_data.city,
        state: doctor_data.state,
        zip: doctor_data.zip,
        distance: doctor_data.distance,
        taxonomy_code: doctor_data.taxonomy_name,
        Lastname: doctor_data.last_name,
        datelabel: datelabel,
        date: appointment_times.appointment_date,
        time: appointment_times.appointment_time,
        provider_id: doctor_data.id,
        appointment_id: appointment_times.appointment_id,
      }
      });
      let gmailuser1 = localStorage.getItem("gmailuser");
      console.log(gmailuser1);
      if(gmailuser1 == null){
    $(document).ready(function(){
  
      $("#1").show();
      $("#2").show();
      $("#3").hide();
      $("#4").hide();$("#login").hide();
      $("#msbmt").hide();
      $("#spin-loader").hide();
      $("#spin-popup").hide();
      console.log("isnide");
    });
  }
  else{
    this.props.history.push({pathname: "/admin/appointments",search: `?provider_id=${this.state.activeDoc.provider_id}&appointment_id=${this.state.activeDoc.appointment_id}`,state: this.state.activeDoc });
  }
  }
  chartemail(){
    $(document).ready(function(){
  
      $("#1").hide();
      $("#2").show();
      $("#3").hide();
      $("#4").show();
      $("#login").hide();
      $("#msbmt").hide();
      $("#spin-loader").hide();
      $("#spin-popup").hide();
      console.log("isnide1234");
    });
  }
  chart(){
    $(document).ready(function(){
  
      $("#btnClick").click(function(event){
        if ($("#1").is(":visible"))
        {
          console.log("isnide2");
          $("#1").hide();
          $("#login").show();
          $("#3").show();
          $("#4").hide();
          $("#2").hide();
          $("#msbmt").show();
          $("#spin-loader").hide();
      $("#spin-popup").hide();
      
        } else {
          console.log("isnide3");
          $("#msbmt").hide();
          $("#1").show();
          $("#2").hide();
          $("#3").hide();
          $("#4").hide();
          $("#login").hide();
        }
        console.log("isnide");
        
        //Your actions here
      });
    });
  //   $('#btnClick').on('click',function(){
  //     console.log("isnide");
  //     if($('#1').css('display')!='none'){
  //       $("#2").show();
  //     }
  //     else if($('#2').css('display')!='none'){
  //       $("#2").hide();
  //         // $('#1').show().siblings('div').hide();
  //     }
  // });
  }
  componentWillMount() {
    
    if (window.Chart) {
      parseOptions(Chart, chartOptions());
    }
  }
  componentDidUpdate(e) {
        $("#add").hide();
        $("#remove").hide();
        $("#exist").hide();
    // document.documentElement.scrollTop = 0;
    // document.scrollingElement.scrollTop = 0;
    // this.refs.mainContent.scrollTop = 0;
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  render() {
    let favcard = localStorage.getItem("favcard");
    let email = localStorage.getItem("gmailuser");
    let session_id = localStorage.getItem("session_id");
    // console.log(session_id);
    if (!this.state.doctor_list.nearby) {
      return null;
    }
    

    const responseGoogle = (response) => {
      console.log("google response: ",response);
      
      if(response.Qt.Ad != ''){
        localStorage.setItem("gmailuser", response.Qt.Ad);
        localStorage.setItem("profile", response.profileObj.imageUrl);
        this.props.history.push({pathname: "/admin/appointments",search: `?provider_id=${this.state.activeDoc.provider_id}&appointment_id=${this.state.activeDoc.appointment_id}`,state: this.state.activeDoc });
      }
      console.log(this.state.isAuthenticated);
    }
    return (
      <>
        <AdminNavbar
    {...this.props}
    brandText={this.getBrandText(this.props.location.pathname)}
  />
        {/* Page content */}
       
        <div className="main-content-block">
        <Container className="" fluid>
          <div id="add" className="alert alert-success">
          Added in favourite list
          </div>
          <div id="remove" className="alert alert-primary">
          Removed from favourite list
          </div>
          <div id="exist" className="alert alert-primary">
          It's already favourited
          </div>
          <div className="filter-view-block">
          {/* <div className="mdl-btn">
      <Button className="sgnup-btn" type="button"
       onClick={() => { this.toggleModal("exampleModal"); this.chartmain();}}
          
        >
          signup-test
        </Button>
      </div> */}
            <div className="single-fun">
                <button type="button" className="filter">
                  Filter
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="10.667" viewBox="0 0 16 10.667">
                    <path id="Icon_material-filter-list" data-name="Icon material-filter-list" d="M10.722,19.667h3.556V17.889H10.722ZM4.5,9v1.778h16V9Zm2.667,6.222H17.833V13.444H7.167Z" transform="translate(-4.5 -9)" fill="#8e8e93"/>
                  </svg>
                </button>
            </div>

            <div className="single-fun">
                <button type="button" className="icon-wrap-block">
                <svg xmlns="http://www.w3.org/2000/svg" width="11" height="12" viewBox="0 0 11 12">
  <g id="Group_135" data-name="Group 135" transform="translate(-836 -94)">
    <rect id="Rectangle_20" data-name="Rectangle 20" width="3" height="3" transform="translate(836 94)" fill="#8e8e93"/>
    <rect id="Rectangle_23" data-name="Rectangle 23" width="3" height="3" transform="translate(836 98)" fill="#8e8e93"/>
    <rect id="Rectangle_26" data-name="Rectangle 26" width="3" height="4" transform="translate(836 102)" fill="#8e8e93"/>
    <rect id="Rectangle_21" data-name="Rectangle 21" width="3" height="3" transform="translate(840 94)" fill="#8e8e93"/>
    <rect id="Rectangle_24" data-name="Rectangle 24" width="3" height="3" transform="translate(840 98)" fill="#8e8e93"/>
    <rect id="Rectangle_27" data-name="Rectangle 27" width="3" height="4" transform="translate(840 102)" fill="#8e8e93"/>
    <rect id="Rectangle_22" data-name="Rectangle 22" width="3" height="3" transform="translate(844 94)" fill="#8e8e93"/>
    <rect id="Rectangle_25" data-name="Rectangle 25" width="3" height="3" transform="translate(844 98)" fill="#8e8e93"/>
    <rect id="Rectangle_28" data-name="Rectangle 28" width="3" height="4" transform="translate(844 102)" fill="#8e8e93"/>
  </g>
</svg>

                </button>
            </div>


            <div className="single-fun">
                <button type="button" className="icon-wrap-block">
                <svg id="Icon_awesome-list-ul" data-name="Icon awesome-list-ul" xmlns="http://www.w3.org/2000/svg" width="14.455" height="11.745" viewBox="0 0 14.455 11.745">
                  <path id="Icon_awesome-list-ul-2" data-name="Icon awesome-list-ul" d="M1.355,3.375A1.355,1.355,0,1,0,2.71,4.73,1.355,1.355,0,0,0,1.355,3.375Zm0,4.517A1.355,1.355,0,1,0,2.71,9.247,1.355,1.355,0,0,0,1.355,7.892Zm0,4.517A1.355,1.355,0,1,0,2.71,13.765,1.355,1.355,0,0,0,1.355,12.41ZM14,12.861H4.969a.452.452,0,0,0-.452.452v.9a.452.452,0,0,0,.452.452H14a.452.452,0,0,0,.452-.452v-.9A.452.452,0,0,0,14,12.861Zm0-9.035H4.969a.452.452,0,0,0-.452.452v.9a.452.452,0,0,0,.452.452H14a.452.452,0,0,0,.452-.452v-.9A.452.452,0,0,0,14,3.827Zm0,4.517H4.969a.452.452,0,0,0-.452.452v.9a.452.452,0,0,0,.452.452H14a.452.452,0,0,0,.452-.452V8.8A.452.452,0,0,0,14,8.344Z" transform="translate(0 -3.375)" fill="#8e8e93"/>
                </svg>
                </button>
            </div>

            <div className="single-fun">
                <button type="button" className="icon-wrap-block">
                <svg id="Icon_awesome-map-marker-alt" data-name="Icon awesome-map-marker-alt" xmlns="http://www.w3.org/2000/svg" width="10.841" height="14.455" viewBox="0 0 10.841 14.455">
                  <path id="Icon_awesome-map-marker-alt-2" data-name="Icon awesome-map-marker-alt" d="M4.864,14.164C.761,8.217,0,7.606,0,5.421a5.421,5.421,0,0,1,10.841,0c0,2.186-.761,2.8-4.864,8.743a.678.678,0,0,1-1.114,0Zm.557-6.484A2.259,2.259,0,1,0,3.162,5.421,2.259,2.259,0,0,0,5.421,7.679Z" fill="#8e8e93"/>
                </svg>

                </button>
            </div>

          </div>

          <div id="drs-grid" className="doctors-list">
          <Row className="align-items-top">
         
        {this.state.doctor_list.nearby.providers.map((doctor_data,appointments, idx) => {
            return (
              <Col  key={idx} col="12" xl="3" md="3" sm="4" xs="12" className="c-grid">
              <div className="doctor-block-wrap">
                  <div className="doctor-block">
                  {/* <Link to="/admin/doctor_profile" className="form-links"> */}
                  {/* <div>{favcard}</div> */}
                  <img onClick={() => { this.chartmain1(doctor_data, appointments, "");}}  className="img-fluid" src={require('./../assets/img/doctors/dr1.jpg')}  alt="fireSpot"/>            
                    {/* <img className="img-fluid" src={require('./../assets/img/doctors/dr1.jpg')}  alt="fireSpot"/> */}
                    {/* </Link> */}
                    
                    {favcard != null ? (
                      
                      <span  className="fav">
                      {doctor_data.favourite == true ? (
                      <img className="img-fluid" onClick={email == null ?  () => { this.toggleModal("exampleModal");} : () => { this.un_favmain(doctor_data);}}  src={require('./../assets/img/doctors/heart.svg')}  alt="fireSpot"/>
                    ) : (
                      <img className="img-fluid" onClick={email == null ?  () => {this.toggleModal("exampleModal"); this.favmain(doctor_data);}  : () => { this.favmain(doctor_data);}}  src={require('./../assets/img/doctors/grey-heart.svg')}  alt="fireSpot"/>
                    )}
                      
                      
                      </span>
                    ) : (
                      <span  className="fav">
                    {doctor_data.favourite == true ? (
                      
                    <img className="img-fluid" onClick={() => { this.toggleModal("exampleModal"); this.favmain(doctor_data);}}  src={require('./../assets/img/doctors/heart.svg')}  alt="fireSpot"/>
                  ) : (
                    <img className="img-fluid" onClick={() => { this.toggleModal("exampleModal"); this.favmain(doctor_data);}}  src={require('./../assets/img/doctors/vectorpaint.png')}  alt="fireSpot"/>
                  )}
                    
                    
                    </span>
                    )}
                    
                    
                    {doctor_data.is_sponsored == true ? (
                      <div className="dr-sponsored">
                    <h3>SPONSORED</h3>
                    </div>
                  ) : (
                    ""
                  )}
                      
                    </div>
                  
                  <div className="doctor-info">
                      <h3 key={idx}>{doctor_data.first_name} {doctor_data.last_name}</h3>
                      <span>{doctor_data.taxonomy_name}</span>
                      <address key={idx}>{doctor_data.address1}, {doctor_data.address2}, {doctor_data.city} {doctor_data.zip}</address>
                      <p>{doctor_data.distance} Miles</p>

                      <div className="ratings">
                        <div className="rating-inner">
                          <div className="star-ratings">
                             <StarRatingComponent 
                            name="rate2" editing={false}
                            starCount={doctor_data.ratings} 
                            starColor="#f28a18" 
                            emptyStarColor="#000"
                            value={doctor_data.ratings}
                            
                          />
                          </div>

                        </div>
                        <div className="ratings">
                        <div className="reviews">
                          <p>Reviews : {doctor_data.reviews}</p>
                        </div>
                        </div>
                      </div>
                      <div className="doctor-bottom">
                        <div className="buttons-grp">
                        <p>Today</p> 

                    {doctor_data.appointments.map((appointment_times, idx_at) => {
                    return ( 
                    <div className="single-btn">                           
                      <a onClick={() => { this.toggleModal("exampleModal"); this.chartmain(doctor_data, appointment_times, "");}} class="active" role="button" aria-pressed="true" key={idx_at}>{appointment_times.appointment_time}</a>
                            
                        </div>            
                            )
                      })}
                        </div>
                      </div>
                  </div>
              </div>
            </Col>
            )
          })}
           
          
          </Row>
          
          <div className="today-list">
            <div className="today-inner">
                  <h1>Today</h1>

                   {this.state.doctor_list.recommend1.providers.map((doctor_data, idx) => {
            return (
              <Col  key={idx} col="12" xl="3" md="3" sm="4" xs="12" className="c-grid">
              <div className="doctor-block-wrap">
                  <div className="doctor-block">
                  <Link to="/admin/doctor_profile" className="form-links">
                    <img className="img-fluid" src={require('./../assets/img/doctors/dr1.jpg')}  alt="fireSpot"/>
                    </Link>
                    <a href="" className="fav">
                    <img className="img-fluid" src={require('./../assets/img/doctors/heart.svg')}  alt="fireSpot"/>
                    </a>
                    <div className="dr-sponsored">
                      {doctor_data.is_sponsored == true ? (
                    <h3>SPONSORED</h3>
                  ) : (
                    ""
                  )}
                    </div>
                  </div>
                  <div className="doctor-info">
                      <h3 key={idx}>{doctor_data.first_name} {doctor_data.last_name}</h3>
                      <span>{doctor_data.taxonomy_name}</span>
                      <address key={idx}>{doctor_data.address1}, {doctor_data.address2}, {doctor_data.city} {doctor_data.zip}</address>
                      <p>{doctor_data.distance} Miles</p>

                      <div className="ratings">
                        <div className="rating-inner">
                        

                        <div className="star-ratings">
                             <StarRatingComponent 
                            name="rate2" editing={false}
                            starCount={doctor_data.ratings} 
                            starColor="#f28a18" 
                            emptyStarColor="#000"
                            value={doctor_data.ratings}
                            
                          />
                          </div>

                        </div>
                        <div className="reviews">
                          <p>Reviews : {doctor_data.reviews}</p>
                        </div>
                      </div>
                      <div className="doctor-bottom">
                        <div className="buttons-grp">
                        <p>Today</p> 
                          {doctor_data.appointments.map((appointment_times, idx_at) => {
                    return ( 
                    <div className="single-btn">                           
                            <a onClick={() => { this.toggleModal("exampleModal"); this.chartmain(doctor_data, appointment_times, 'Today');}} class="active" role="button" aria-pressed="true" key={idx_at}>{appointment_times.appointment_time}</a>
                            
                        </div>
            
                            )
                      })}
                        </div>
                      </div>
                  </div>
              </div>
            </Col>
            )
          })}
           
                  <a href="#" className="view-all">View-all</a>
            </div>
            
        
          </div>

          <div className="tomorrow-list">
           

            <div className="tomorrow-inner">
            <h1>Tomorrow</h1>
            <a href="#" className="view-all">View-all</a>
            </div>
            <div className="row align-items-top">
                  {this.state.doctor_list.recommend2.providers.map((doctor_data, idx) => {
            return (
              
              <Col  key={idx} col="12" xl="3" md="3" sm="4" xs="12" className="c-grid">
              <div className="doctor-block-wrap">
                  <div className="doctor-block">
                  <Link to="/admin/doctor_profile" className="form-links">
                    <img className="img-fluid" src={require('./../assets/img/doctors/dr1.jpg')}  alt="fireSpot"/>
                    </Link>
                    <a href="" className="fav">
                    <img className="img-fluid" src={require('./../assets/img/doctors/heart.svg')}  alt="fireSpot"/>
                    </a>
                    
                      {doctor_data.is_sponsored == true ? (
                        <div className="dr-sponsored">
                    <h3>SPONSORED</h3>
                    </div>
                  ) : (
                    ""
                  )}
                    </div>
                  
                  <div className="doctor-info">
                      <h3 key={idx}>{doctor_data.first_name} {doctor_data.last_name}</h3>
                      <span>{doctor_data.taxonomy_name}</span>
                      <address key={idx}>{doctor_data.address1}, {doctor_data.address2}, {doctor_data.city} {doctor_data.zip}</address>
                      <p>{doctor_data.distance} Miles</p>

                      <div className="ratings">
                        <div className="rating-inner">
                        

                        <div className="star-ratings">
                             <StarRatingComponent 
                            name="rate2" editing={false}
                            starCount={doctor_data.ratings} 
                            starColor="#f28a18" 
                            emptyStarColor="#000"
                            value={doctor_data.ratings}
                            
                          />
                          </div>

                        </div>
                        <div className="reviews">
                          <p>Reviews : {doctor_data.reviews}</p>
                        </div>
                      </div>
                      <div className="doctor-bottom">
                        <div className="buttons-grp">
                        <p>Today</p>
                         {doctor_data.appointments.map((appointment_times, idx_at) => {
                    return ( 
                    <div className="single-btn">                           
                            <a onClick={() => { this.toggleModal("exampleModal"); this.chartmain(doctor_data, appointment_times, 'Tomorrow');}} class="active" role="button" aria-pressed="true" key={idx_at}>{appointment_times.appointment_time}</a>
                            
                        </div>
            
                            )
                      })}
                          
                        </div>
                      </div>
                  </div>
              </div>
            </Col>
           
            )
          })}
           </div>

                
            </div>
            
        
          </div>



           
         
        </Container>
        </div>
      
        <div className="map-section ">
              <p>Map</p>           
          </div>

          

      
      

        <Modal
          className="modal-dialog-centered"
          isOpen={this.state.exampleModal}
          toggle={() => this.toggleModal("exampleModal")}
        >

        {/* <div id ="5" className="modal-header m-header">
            <div className="mdl-nm">
            <h5 className="modal-title" id="exampleModalLabel">
              Added favourite list
            </h5>
            </div>
          </div>
           <div id ="6" className="modal-header m-header">
            <div className="mdl-nm">
            <h5 className="modal-title" id="exampleModalLabel">
            Removed from favourite list
            </h5>
            </div>
          </div> */}

          <div id ="2" className="modal-header m-header">
            <div className="mdl-nm">
            <h5 className="modal-title" id="exampleModalLabel">
            Sign up 
            </h5>
            </div>

          </div>
          <div id ="login" className="modal-header m-header">
            <div className="mdl-nm">
            <h5 className="modal-title" id="exampleModalLabel">
            Sign in 
            </h5>
            </div>

          </div>
          <div  className="modal-body m-body">
            <div className="m-content" id ="1">
                <h1>Complete your appointment with Dr. {this.state.activeDoc.Firstname} {this.state.activeDoc.Lastname}</h1>

                <div className="social-btns">
                <div className="google-btn">
                    
                    <GoogleLogin
        clientId="495161513893-3tk7gq2uhsoesv2gs757akal2nf05asb.apps.googleusercontent.com" //CLIENTID NOT CREATED YET
        buttonText="Sign in with mail"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
          />
                      {/* <img className="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"/> */}
                    
                    {/* <p className="btn-text">
                     
      </p> */}
                  </div>

                  <div className="email-box apple-box">
                  <div className="email-ic">
                  <img className="facebook-icon app-icon" src={require("./../assets/img/icons/apple-black.svg")}  />
                  </div>
                  <div className="email-btn-txt" >
                    <p>Sign up with Apple</p>
                  </div>
                  


                </div>

                  <div className="email-box">
                  <div className="email-ic">
                  <img className="facebook-icon" src={require("./../assets/img/icons/email.png")}  />
                  </div>
                  <div className="email-btn-txt" id="btnClickemail" onClick={() => {this.chartemail();}}>
                    <p>Sign up with email</p>
                  </div>
                  


                </div>

                
                <div className="al-acc" id="btnClick">
                  <p>Already have an account?<a onClick={() => this.chart()}>  Sign in </a></p>
                </div>
                </div>
            </div>

            <div id ="4" className="emailform">
            <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <label for="email" className="e-label">Username</label>
                  <input type="username" 
                  className="form-control" 
                  id="username" 
                  required
                  aria-describedby="emailHelp"
                  value={this.state.Username}
                  onChange={evt =>
                  this.setState({
                  Username: evt.target.value
                  })}
                  placeholder="swdev" />
                </div>
                <div className="form-group">
                  <label for="email" className="e-label">Email</label>
                  <input type="email" 
                  className="form-control" 
                  id="email" 
                  required
                  aria-describedby="emailHelp"
                  value={this.state.Email}
                  onChange={evt =>
                  this.setState({
                    Email: evt.target.value
                  })}
                  placeholder="email@Host.com" />
                </div>
                
                <div className="form-group">
                  <label for="pwd" className="e-label">Create a Password</label>
                  <input 
                  required
                  type="password" 
                  className="form-control" 
                  id="pwd" 
                  placeholder=""
                  value={this.state.Password}
                  onChange={evt =>
                  this.setState({
                  Password: evt.target.value
                  })
                  }
                   />
                </div> 
                <div className="form-group">
                  <label for="firstname" className="e-label">First Name</label>
                  <input type="firstname" 
                  className="form-control" 
                  id="firstname" 
                  required
                  aria-describedby="emailHelp"
                  value={this.state.Firstname}
                  onChange={evt =>
                  this.setState({
                    Firstname: evt.target.value
                  })}
                  placeholder="Firstname" />
                </div>
                <div className="form-group">
                  <label for="lastname" className="e-label">Last Name</label>
                  <input type="lastname" 
                  className="form-control" 
                  id="lastname" 
                  required
                  aria-describedby="emailHelp"
                  value={this.state.Lastname}
                  onChange={evt =>
                  this.setState({
                    Lastname: evt.target.value
                  })}
                  placeholder="Firstname" />
                </div>
                <div className="tip">
                  <small>• Minimum 8 characters (no spaces)</small>
                  <small>* Letters, numbers and special characters</small>
                </div>
                <div className="tip">
                  {this.state.Validation}
                </div>
                <div className="modal-cancel-btn" id="mcncl11">
            <Button
              data-dismiss="modal"
              type="submit"
              className="cncl-btn"
            >
              Submit
            </Button>
            </div>
            </form>

            </div>
            <div id ="3" className="emailform">
            <form onSubmit={this.onSubmitlogin}>
                <div className="form-group">
                  <label for="email" className="e-label">Email</label>
                  <input type="email" 
                  className="form-control" 
                  id="email" 
                  required
                  aria-describedby="emailHelp"
                  value={this.state.Usernamelogin}
                  onChange={evt =>
                  this.setState({
                  Usernamelogin: evt.target.value
                  })}
                  placeholder="email@Host.com" />
                </div>
                {/* <div className="form-group">
                  <label for="cnfrmemail" className="e-label">Confirm your email</label>
                  <input type="email" className="form-control" id="cnfrmemail" aria-describedby="emailHelp" placeholder="email@Host.com" />
                </div> */}
                <div className="form-group">
                  <label for="pwd" className="e-label">Enter a Password</label>
                  <input 
                  required
                  type="password" 
                  className="form-control" 
                  id="pwd" 
                  placeholder=""
                  value={this.state.Passwordlogin}
                  onChange={evt =>
                  this.setState({
                  Passwordlogin: evt.target.value
                  })
                  }
                   />
                </div> 
                <div className="tip">
                  {this.state.Validationlogin}
                </div>
               
            <div className="modal-footer">
          <div className="modal-sbmt-btn" id="msbmt">
          {/* <Link to="/admin/appointments" className="form-links">
          <button type="submit" className="btn msbmt-btn"  onClick={() => this.appointmentPopup()}>Sign Up</button>
                    </Link> */}
          
          </div>
          
          <div className="modal-cancel-btn" id="mcncl11">
            <Button
              data-dismiss="modal"
              type="submit"
              className="cncl-btn"
            >
              Submit
            </Button>
            </div>
            <div className="modal-cancel-btn" id="mcncl">
            <Button
              data-dismiss="modal"
              type="button"
              onClick={() => this.toggleModal("exampleModal")}
              className="cncl-btn"
            >
              Cancel
            </Button>
            </div>

            <div className="" id="spin-popup">
          <button type="submit" data-dismiss="modal"  type="button" className="btn cls-btn">Close</button>
          </div>
            
            
          </div>
            </form>

            </div>

            <div className="appoint-redirect" id="spin-loader">
            <Link to="{{
              pathname: '/admin/appointments',
              state: this.state.activeDoc 
              }}" className="form-links"> </Link>
                {/* <h1>We are booking your appointment.This could take a few seconds please be patient.</h1>
                <div className="spin-img">
                <img className="img-fluid" src={require('./../assets/img/icons/spinner.gif')}  alt="fireSpot"/>
                </div> */}
            </div>
          </div>
          
          
        </Modal>

      </>
      
    );
  }
}




export default Index;