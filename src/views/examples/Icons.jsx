
import React from "react";
import ReacrDOM from 'react-dom';
import { Route, Switch } from "react-router-dom";


// react component that copies the given text inside your clipboard
import OwlCarousel, { Options } from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import $ from 'jquery';
import "./icons.scss";
import AdminNavbar1 from "./../../components/Navbars/AdminNavbar1.jsx";
import AdminFooter from "components/Footers/AdminFooter.jsx";
import Sidebar from "components/Sidebar/Sidebar.jsx";
import AppointmentSlot from "./../../components/AppointmentSlot/AppointmentSlot";
import routes from "routes.js";


// reactstrap components
import {
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";
// core components
import Header from "components/Headers/Header.jsx";

import axios from 'axios';
import moment from 'moment';
import ReactTimeslotCalendar from 'react-timeslot-calendar';
import StarRatingComponent from 'react-star-rating-component';

const API_URL = 'http://getys.zapto.org/api';

const corsoptions = {
  headers: { 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' }
};
const options = {

  responsiveClass: true,
  responsive: {
    0: {
      items: 2,
    },
    400: {
      items: 3,
    },
    700: {
      items: 3,
    },


    1000: {
      items: 3,

    }
  },
};

const currentDate = moment(new Date()).format("L")


class Icons extends React.Component {

  constructor(props) {
    super(props);
    let newDate = new Date()
    let date = newDate.getDate();
    this.state = { date: new Date() };
    this.state = {
      persons: [], appointmentSlot: [], provider_details: [], provider_obj:

      {
        "success": true,
        "result": {
          "id": "4537453260817604",
          "first_name": "KELLY",
          "last_name": "JACKSON-KING",
          "zip": "89109",
          "address1": "3196 S MARYLAND PKWY",
          "address2": null,
          "city": "LAS VEGAS",
          "state": "NV",
          "credential": "MD",
          "taxonomy_code": "207V00000X",
          "latitude": "36.1319199",
          "longitude": "-115.1358324",
          "appointments": [
            {
              "appointment_id": 23422,
              "appointment_time": "09:30",
              "appointment_date": "2019-12-26"
            },
            {
              "appointment_id": 23423,
              "appointment_time": "14:30",
              "appointment_date": "2019-12-26"
            },
            {
              "appointment_id": 23424,
              "appointment_time": "15:30",
              "appointment_date": "2019-12-26"
            },
            {
              "appointment_id": 23425,
              "appointment_time": "16:30",
              "appointment_date": "2019-12-26"
            },
            {
              "appointment_id": 23426,
              "appointment_time": "17:00",
              "appointment_date": "2019-12-26"
            }
          ],
          "rating": {
            "overall": 4.5,
            "wait_times": 4,
            "bedside_manner": 4.8
          },
          "reviews": {
            "total": 1,
            "reviews": [
              {
                "overall": 0,
                "wait_times": 0,
                "bedside_manner": 0,
                "content": "Not good",
                "created_at": "",
                "created_by": "User"
              }
            ]
          },
          "photos": [],
          "professional_statement": ""
        },
        "targetUrl": "",
        "error": {
          "code": "",
          "message": ""
        },
        "message_id": "dca47f4c-6fce-41ca-9c3f-4316d96d84d7"
      }

    };


  }

  componentDidMount() {
    //console.log(this.props);
    //console.log(this.props.history.location.state.provider_id);
    let providerIdArray = this.props.match.params.providerId.split('-');
    let providerId = providerIdArray[ 5 ];
    axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
        const persons = res.data;
        this.setState({ persons });
        console.log("get from state persons", this.state.persons);

      })



    let url = `https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/provider/${providerId}?=detail`;
    const requestOptions = {
      method: "GET",
      headers: { "Content-Type": "application/json", 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' },
    };
    // return (
    fetch(`${url}`, requestOptions)
      .then(res => {
        res.json().then(json => {
          console.log("providers: ", json.result);
          let provider_details_ob = json.result;
          let appointmentSlot = json.result.appointments.slice(0, 3)
          this.setState({
            appointmentSlot
          });
          this.setState({
            provider_details: provider_details_ob
          });

        });
      })
    // );

    axios.get(`https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/appointment/open?providerid=${providerId}`, {
      headers: {
        'Content-Type': 'application/json',
        'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM'
      }
    })
      .then(res => {
        console.log("appoinment data: ", res.data.result);
      })


  }


  componentWillUnmount() {
    console.log("Componets unmount")
  }
  componentDidUpdate(e) {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    // this.refs.mainContent.scrollTop = 0;
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[ i ].layout + routes[ i ].path
        ) !== -1
      ) {
        return routes[ i ].name;
      }
    }
    return "Brand";
  };

  getMoreSlot(index) {
    console.log("index: ", index);
    let appointmentSlot = this.state.provider_details.appointments
    this.setState({
      appointmentSlot
    })
  }

  selectSlot(index) {

  }

  render() {


    if (!this.state.provider_details.rating) {
      return null;
    }
    if (!this.state.provider_details.reviews.reviews) {
      return null;
    }

    return (
      <>




        <Sidebar
          {...this.props}
          routes={routes}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("assets/img/logo.svg"),
            imgAlt: "..."
          }}
        />
        <div className="main-content d-flex align-items-top" ref="mainContent">
          {/* <AdminNavbar
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
          /> */}
          {/* <Switch>{this.getRoutes(routes)}</Switch> */}
          <AdminNavbar1
            {...this.props}
            brandText={this.getBrandText(this.props.location.pathname)}
          />
          {/* Page content */}



          <div className="dr-prfle-wrap">
            <Container className="" fluid>


              <div className="dr-prfl-main-block">

                <Row className="align-items-top">

                  <div>
                  </div>





                  <Col xl="12">

                    <div className="dr-prfle-info">
                      <div className="dr-prfle-img">
                        <img className="img-fluid" src={require('./../../assets/img/doctors/dr-prfle.jpg')} alt="" />
                      </div>
                      <div className="dr-prfle-right-info">
                        <h3>Dr. {this.state.provider_details.first_name} {this.state.provider_details.last_name} </h3>
                        <h4>000-000-0000</h4>
                        <p>Live wait time: <span>Under 10 Minutes</span></p>
                        <address> address : {this.state.provider_details.address1} {this.state.provider_details.address2}</address>

                      </div>
                    </div>
                  </Col>

                </Row>

              </div>
              <div className="dr-main-block">
                <div className="apointment-book">
                  <p className="ap-label">Book now:</p>
                  <ul className="ap-btns">
                    <li>
                      <div class="form-group">
                        <input type="checkbox" id="morning" />
                        <label for="morning">Morning <span>(8AM - 12PM)</span></label>
                      </div>
                    </li>

                    <li>
                      <div class="form-group">
                        <input type="checkbox" id="afternoon" />
                        <label for="afternoon">Afternoon <span>(12AM - 4PM)</span></label>
                      </div>
                    </li>

                    <li>
                      <div class="form-group">
                        <input type="checkbox" id="evening" />
                        <label for="evening">Evening <span>(4AM - 7PM)</span></label>
                      </div>
                    </li>
                  </ul>

                  <Row className="align-items-top"></Row>
                </div>
                <div>
                  {/* <ReactTimeslotCalendar
                  initialDate={moment().format()}

                  timeslots={[
                    [ '9:15' ],
                    [ '10' ],
                    [ '18:15' ],
                  ]}
                  maxTimeslots={[ 1 ]}
                  ignoreWeekends={
                    [ 'sundays' ]
                  }
                  disabledTimeslots={[
                    {
                      startDate: 'February 24th 2020, 10:00:00 AM',
                      format: 'MMMM Do YYYY, h:mm:ss A',
                    } ]}
                /> */}
                </div>


                <OwlCarousel
                  className="owl-theme appointment-table"
                  items={30}
                  nav={'true'}
                  dots={'false'}
                  {...options}

                >

                  {this.state.provider_details.appointments.map((appointment, i) => {
                    return (
                      <>
                        <div className={'item'}>
                          <div className="date-head">
                            {moment(currentDate).isSame(appointment.appointment_date) ? <h3>Today</h3> : <h3>{moment(appointment.appointment_date).format("MMM D")}</h3>}
                            <h4>{moment.weekdays(appointment.appointment_date)}</h4>
                          </div>
                          <div className="ap-dates">
                            <ul>
                              {/* <li>
                                <a href="#" class="active" role="button" aria-pressed="true">9:00 AM</a>
                              </li> */}
                              {this.state.appointmentSlot.map((appointment, idx) => {
                                return (
                                  <>
                                    <AppointmentSlot i={idx} time={appointment.appointment_time} />
                                  </>
                                )
                              })}
                              <li>
                                <a class="" role="button" aria-pressed="true" onClick={() => this.getMoreSlot(i)}>More</a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </>
                    )
                  })}

                  {/* <div className={'item'}>
                    <div className="date-head">
                      <h3>Jan 14</h3>
                      <h4>Tuesday</h4>
                    </div>
                    <div className="ap-dates">
                      <ul>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:00 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">More</a>
                        </li>
                      </ul>

                    </div>

                  </div> */}

                  {/* <div className={'item'}>
                    <div className="date-head">
                      <h3>Jan 15</h3>
                      <h4>Wednesday</h4>
                    </div>
                    <div className="ap-dates">
                      <ul>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:00 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">More</a>
                        </li>
                      </ul>

                    </div>
                  </div> */}

                  {/* <div className={'item'}>
                    <div className="date-head">
                      <h3>Jan 16</h3>
                      <h4>Thursday</h4>
                    </div>
                    <div className="ap-dates">
                      <ul>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:00 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">More</a>
                        </li>
                      </ul>

                    </div>
                  </div> */}

                  {/* <div className={'item'}>
                    <div className="date-head">
                      <h3>Jan 17</h3>
                      <h4>Friday</h4>
                    </div>
                    <div className="ap-dates">
                      <ul>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:00 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">9:15 AM</a>
                        </li>
                        <li>
                          <a href="#" class="" role="button" aria-pressed="true">More</a>
                        </li>
                      </ul>

                    </div>


                  </div> */}
                </OwlCarousel>

                <div className="dr-prfle-review">
                  <div className="review-title-block">
                    <h1>Reviews</h1>
                    <span>100
                      <svg xmlns="http://www.w3.org/2000/svg" width="19.867" height="17.5" viewBox="0 0 19.867 17.5">
                        <path id="Icon_awesome-comment" data-name="Icon awesome-comment" d="M9.429,2.25C4.221,2.25,0,5.679,0,9.911a6.8,6.8,0,0,0,2.1,4.814A9.37,9.37,0,0,1,.081,18.253a.293.293,0,0,0-.055.32.288.288,0,0,0,.269.177,8.423,8.423,0,0,0,5.178-1.893,11.232,11.232,0,0,0,3.956.715c5.208,0,9.429-3.429,9.429-7.661S14.636,2.25,9.429,2.25Z" transform="translate(0.51 -1.75)" fill="none" stroke="#fa8c15" stroke-width="1" />
                      </svg>
                    </span>
                  </div>
                  <div className="dr-rating-inner">
                    <Row className="align-items-center">
                      <Col xl="4" md="4">
                        <div className="rvw-info">
                          <label>Overall</label>
                          <p> {this.state.provider_details.rating.overall}  </p>
                          <div className="star-ratings">
                            <StarRatingComponent
                              name="rate2" editing={false}
                              starCount={this.state.provider_details.rating.overall}
                              starColor="#f28a18"
                              emptyStarColor="#000"
                              value={5}

                            />
                          </div>
                        </div>
                      </Col>
                      <Col xl="8" md="8">
                        <div className="wait-time-block">
                          <label>Wait times</label>
                          <div className="rating-wrap">
                            <span> {this.state.provider_details.rating.wait_times}</span>
                            <div className="star-ratings">
                              <StarRatingComponent
                                name="rate2" editing={false}
                                starCount={this.state.provider_details.rating.wait_times}
                                starColor="#f28a18"
                                emptyStarColor="#000"
                                value={5}

                              />
                            </div>
                          </div>
                        </div>
                        <div className="personality-block">
                          <label>Personality</label>
                          <div className="rating-wrap">
                            <span> {this.state.provider_details.rating.bedside_manner}</span>
                            <div className="star-ratings">
                              <StarRatingComponent
                                name="rate2" editing={false}
                                starCount={this.state.provider_details.rating.bedside_manner}
                                starColor="#f28a18"
                                emptyStarColor="black"
                                value={5}

                              />
                            </div>
                          </div>


                        </div>
                      </Col>
                    </Row>
                  </div>

                  <div className="overall-rating">

                    <ul>
                      {this.state.provider_details.reviews.reviews.map((reviews_data, idx) => {
                        return (
                          <div key={idx}>
                            <Row align-items-top>
                              <Col xl="4" md="4" sm="4" >
                                <div className="overall-block">
                                  <label>Overall</label>

                                  <div className="star-ratings">
                                    <StarRatingComponent
                                      name="rate2" editing={false}
                                      starCount={reviews_data.overall}
                                      starColor="#f28a18"
                                      emptyStarColor="#000"
                                      value={5}

                                    />
                                  </div>
                                </div>
                              </Col>
                              <Col xl="4" md="4" sm="4">

                                <div className="personality-rvw-block">
                                  <label>Personality</label>

                                  <div className="star-ratings">
                                    <StarRatingComponent
                                      name="rate2" editing={false}
                                      starCount={reviews_data.bedside_manner}
                                      starColor="#f28a18"
                                      emptyStarColor="black"
                                      value={5}

                                    />
                                  </div>
                                </div>
                              </Col>
                              <Col xl="4" md="4" sm="4">
                                <div className="wait-time-rvw-block">
                                  <label>Wait Time</label>

                                  <div className="star-ratings">
                                    <StarRatingComponent
                                      name="rate2" editing={false}
                                      starCount={reviews_data.wait_times}
                                      starColor="#f28a18"
                                      emptyStarColor="black"
                                      value={5}

                                    />
                                  </div>
                                </div>
                              </Col>

                              <Col xl="12" md="12">
                                <div className="review-txt">


                                </div>
                              </Col>

                            </Row>

                            {reviews_data.created_at}


                            <p>{reviews_data.content} </p>
                          </div>
                        )
                      })}
                    </ul>


                  </div>



                  <div className="read-btn">
                    <a href="" className="">Read more reviews</a>
                  </div>

                </div>



                <div className="photos-block">
                  <h1>Photos</h1>

                  <ul className="pht-grid">
                    <li>
                      <a href="">
                        <img src="" />
                      </a>
                    </li>
                    <li>
                      <a href="">
                        <img src="" />
                      </a>
                    </li>
                    <li>
                      <a href="">
                        <img src="" />
                      </a>
                    </li>
                    <li>
                      <a href="">
                        <img src="" />
                      </a>
                    </li>
                    <li>
                      <a href="">
                        <img src="" />
                      </a>
                    </li>
                  </ul>
                </div>

                <div className="stmnt-block">
                  <h1>Professional Statement</h1>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco poriti laboris nisi ut</p>
                </div>

              </div>
            </Container>
          </div>

          <div className="map-section index-map">
            <p>Map</p>
          </div>
          {/* <Container fluid> */}
          <AdminFooter />
          {/* </Container> */}
        </div>






      </>
    );
  }
}

export default Icons;
