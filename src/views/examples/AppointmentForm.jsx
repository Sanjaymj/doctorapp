
import React from "react";
import ReacrDOM from 'react-dom';
import { Route, Switch } from "react-router-dom";
import ReactDatetime from "react-datetime";
import Chart from "chart.js";

// react component that copies the given text inside your clipboard
import OwlCarousel, { Options } from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import $ from 'jquery';
import "./AppointmentForm.scss";
import AdminNavbar1 from "./../../components/Navbars/AdminNavbar1.jsx";
import AdminFooter from "components/Footers/AdminFooter.jsx";
import Sidebar from "components/Sidebar/Sidebar.jsx";
import Dropdown from 'react-bootstrap/Dropdown';
import routes from "routes.js";
import DatePicker from 'react-date-picker';


// reactstrap components
import {
  
  Button,
  Card,
  CardHeader,
  CardBody,
  Container,
  Row,
  Col,
  FormGroup,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  UncontrolledTooltip, 
  Input,  
  Modal
} from "reactstrap";
// core components
import {
  chartOptions,
  parseOptions,
  chartExample1,
  chartExample2
} from "variables/charts.jsx";
import Header from "components/Headers/Header.jsx";






class AppointmentForm extends React.Component {
 
  constructor(props) {
    super(props);
    this.state = {
      Firstname: '',
      Lastname: '',
      provider_id:'',
      appointment_id:'',
      adate:"",
      time:"",
      address1:'',
      address2:'',
      city:'',
      state:'',
      zip:'',
      distance:'',
      taxonomy_code:'',
      nochecked: false,
      notes:'',
      yeschecked: true,
      check: false,
    selectvisitValue: '',
    selectvisitreasonValue:'',
    selectinsuranceValue: '',
    insurancreason: [],
    visitreason: [],
    activeNav: 1,
    startDate:  new Date(),
    endDate: '',
    firstname:'',
    zipcode:'',
    address:'',
    gender:'',
    insurance:'',
    email:'',
    Username: "",
    Password: "",
    Validation: "",
    Usernamelogin: "",
    Passwordlogin: "",
    Validationlogin: "",
    ForgetValidation: "",
    Useremail: "",
    Message: "",
    isloaded1: true,
    activeNav: 1,
    chartExample1Data: "data1",
    exampleModal: false,
    phone:'',
    radioChecked: 'No',
    chartExample1Data: "data1",
    exampleModal: false,
    date: new Date(),
    checked: false,
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.handlevisitChange = this.handlevisitChange.bind(this);
    this.handleinsuranceChange = this.handleinsuranceChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheck = this.handleCheck.bind(this);
    // this.onSubmitlogin = this.onSubmitlogin.bind(this);
    // this.onForgetSubmit = this.onForgetSubmit.bind(this);
    }
    toggleModal = state => {
      console.log("isnide");
      this.setState({
        [state]: !this.state[state]
      });
    };
  
    
    toggleNavs = (e, index) => {
      e.preventDefault();
      this.setState({
        activeNav: index,
        chartExample1Data:
          this.state.chartExample1Data === "data1" ? "data2" : "data1"
      });
      let wow = () => {
        console.log(this.state);
      };
      wow.bind(this);
      setTimeout(() => wow(), 1000);
      // this.chartReference.update();
    };
    
    componentDidMount(){
      
      let providerIdArray = this.props.match.params.providerId.split('-');
      let providerId = providerIdArray[5];
      let appointmentIdArray = this.props.match.params.appointmentId.split('-');
      let appointmentId = appointmentIdArray[5];
      console.log("appointmentId=",appointmentId);

      let url = `https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/provider/${providerId}?=detail`;
      const requestOptions = {
        method: "GET",
        headers: { "Content-Type": "application/json", 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' },
      };
      
      fetch(`${url}`, requestOptions)
        .then(res => {
          res.json().then(json => {
            let provider_details_ob = json.result;
            let adate = "";
            let atime = "";
            provider_details_ob.appointments.map((appointment_times, idx_at) => {
              console.log("appointment_times=",appointment_times);
               if(appointment_times.appointment_id === appointmentId){
                adate = appointment_times.appointment_date;
                atime = appointment_times.appointment_time;
               }
            });
            console.log("provider_details_ob=",provider_details_ob);
            console.log("adate=",adate);

            this.setState({
                Firstname:provider_details_ob.first_name,
                Lastname:provider_details_ob.last_name,
                adate:adate,
                time:atime,
                provider_id:provider_details_ob.id,
                appointment_id:appointmentId,
                address1:provider_details_ob.address1,
                address2:provider_details_ob.address2,
                city:provider_details_ob.city,
                state:provider_details_ob.state,
                zip:provider_details_ob.zip,
                taxonomy_code:provider_details_ob.taxonomy_code,
              });
          });
        });
      
      // this.insurance();
      $(document).ready(function(){
    
        $("#book").hide();
        $("#new").show();
       
      });
      this.setState({
        Validation: ""
        });
    
        let visisturl = "https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/category/visit-reason";
        let insuranceurl = "https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/category/insurance-carrier";
        
        fetch(`${insuranceurl}`, requestOptions)
        .then(res => {
          res.json().then(json => {
            console.log(json.result);
            let insurancreason = json.result;
            this.setState({
              insurancreason: insurancreason
              });

            });
        })

        fetch(`${visisturl}`, requestOptions)
        .then(res => {
          res.json().then(json => {
            console.log(json.result);
            let visitreason = json.result;
            this.setState({
              visitreason: visitreason
              });

            });
        })
        
    }

    appointmentPopup() {
      $(document).ready(function(){
        $("#bookloader").hide();
        $("#new").hide();    
        $("#book").show();

        
        console.log("isnide");
      });
    }
    chartmain(){
      $(document).ready(function(){
    
        $("#1").show();
        $("#2").show();
        $("#3").hide();
        $("#4").hide();
        $("#msbmt").hide();
        $("#spin-loader").hide();
        $("#spin-popup").hide();
        console.log("isnide");
      });
    }
    chartemail(){
      $(document).ready(function(){
    
        $("#1").hide();
        $("#2").show();
        $("#3").hide();
        $("#4").show();
        $("#msbmt").hide();
        $("#spin-loader").hide();
        $("#spin-popup").hide();
        console.log("isnide1234");
      });
    }
    chart(){
      $(document).ready(function(){
    
        $("#btnClick").click(function(event){
          if ($("#1").is(":visible"))
          {
            console.log("isnide2");
            $("#1").hide();
            $("#2").show();
            $("#3").show();
            $("#4").hide();
            $("#msbmt").show();
            $("#spin-loader").hide();
        $("#spin-popup").hide();
        
          }
          else
          {
            console.log("isnide3");
            $("#msbmt").hide();
            $("#1").show();
            $("#2").hide();
            $("#3").hide();
            $("#4").hide();
          }
          console.log("isnide");
          
          //Your actions here
        });
      });
    //   $('#btnClick').on('click',function(){
    //     console.log("isnide");
    //     if($('#1').css('display')!='none'){
    //       $("#2").show();
    //     }
    //     else if($('#2').css('display')!='none'){
    //       $("#2").hide();
    //         // $('#1').show().siblings('div').hide();
    //     }
    // });
    }
    componentWillMount() {
      
      if (window.Chart) {
        parseOptions(Chart, chartOptions());
      }
    }
    componentDidUpdate(e) {
      // document.documentElement.scrollTop = 0;
      // document.scrollingElement.scrollTop = 0;
      // this.refs.mainContent.scrollTop = 0;
    }
  
    toggleModal = state => {
      this.setState({
        [state]: !this.state[state]
      });
    };
    onSubmit(e) {
      
      $(document).ready(function(){
        console.log("isnide1");
        $("#book").hide();
        $("#bookloader").show();
        $("#new").hide();
        
        
      });
     
      let APIToken ="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM";
      let email = "this.state.Username";
      let new_patient = "Yes";
      let insurance = "No";
      let password = "this.state.Password";
      e.preventDefault();
    if( this.state.yeschecked == true){
      new_patient = "Yes";
    }
    else{
      new_patient = "No";
    }
    if(this.state.selectinsuranceValue != ""){
insurance = "Yes";
    }
    else{
      insurance = "No";
    }
      // let environement = ENVIORMENT.devlopmenturl;
      // console.log(environement);
      let url = "https://cors-anywhere.herokuapp.com/http://getys.zapto.org/api/v2/appointment/";
      let gmailuser1 = localStorage.getItem("gmailuser");
      const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json", 'API-Token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJHZXR5cyIsIm5hbWUiOiJIdXkgVnUiLCJqdGkiOiIzNWE4MDc0Yi1kZmI4LTRkN2QtYmVmYi0xNzBiM2E2YTZmZDMiLCJpYXQiOjE1NzgzMzIxOTV9.6Y_lDm-6crxFzXZmEMH41g6vVCnqo6pvOkj2A8a3feM' },
      body: JSON.stringify({ 
      "appointment_id": this.state.appointment_id,
      "appointment_type_id": 5,
      "new_patient": new_patient,
      "simple_name": this.state.firstname,
      "address_line1": this.state.address,
      "address_line2": null,
      "city_state_zip": this.state.zipcode,
      "phone": this.state.phone,
      "dob": "1990-01-01",
      "gender": this.state.gender,
      "patient_email": gmailuser1,
      "provider_id": this.state.provider_id,
      "booking_forself": "Yes",
      "insurance": insurance,
      "visit_reason": this.state.selectvisitValue,
      "visit_reason_id": this.state.selectvisitreasonValue,
      "visit_note": this.state.notes})
      };
      return (
      fetch(`${url}`, requestOptions)
      // .then(res => res.json())
      .then(res => {
        console.log(res);
        res.json().then(json => {
          console.log(json);
          if(json.success  == "true"){
            console.log(json);
            this.setState({
              Validationlogin: json.error.message
              });
          }
          else{
              console.log(json);
              this.setState({
                Validationlogin: ""
                });
                $("#book").show();
                $("#bookloader").hide();
                $("#new").hide();
              $(document).ready(function(){
                console.log("isnidesss1");
                $("#book").show();
                $("#bookloader").hide();
                $("#new").hide();
              });
          }
          });
      //store user details and jwt token in local storage to keep user logged in between page refreshes
      // localStorage.setItem("user", JSON.stringify(user.status));
      // localStorage.setItem("username", JSON.stringify(this.state.Username));
      return email;
      })
      );
      }
      handleCheck = (e) => {
        console.log(e.target.name);
        if(e.target.name == "check"){
          this.setState({
            nochecked: e.target.value,
            yeschecked: false,
          });
        }
        else if(e.target.name == "check1"){
            this.setState({
              yeschecked: e.target.value,
              nochecked: false,
            });
          }
        
      }

  chart(){
    $(document).ready(function(){
  
      $("#btnClick").click(function(event){
        $("#2").show();
        $("#3").show();
        $("#msbmt").show();
        // $("#spin-loader").show();
    $("#spin-popup").hide();
        console.log("isnide");
        
        //Your actions here
      });
    });
  //   $('#btnClick').on('click',function(){
  //     console.log("isnide");
  //     if($('#1').css('display')!='none'){
  //       $("#2").show();
  //     }
  //     else if($('#2').css('display')!='none'){
  //       $("#2").hide();
  //         // $('#1').show().siblings('div').hide();
  //     }
  // });
  }
  componentWillUnmount() {

  }
  componentDidUpdate(e) {
    console.log(this.props.history.location.state);
    
    
  }
  getRoutes = routes => {
    return routes.map((prop, key) => {
      if (prop.layout === "/admin") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      } else {
        return null;
      }
    });
  };
  getBrandText = path => {
    for (let i = 0; i < routes.length; i++) {
      if (
        this.props.location.pathname.indexOf(
          routes[i].layout + routes[i].path
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Appoitment";
  };
  handlevisitChange = (e) => {
    


    this.setState({selectvisitValue:$( "#myselect option:selected" ).text()});
    this.setState({selectvisitreasonValue:e.target.value});
    
  }
 
handleinsuranceChange(e){
  console.log(e);
  this.setState({selectinsuranceValue:e.target.value});
  
}
goAppointmentChangeDate(providerId){
    var uuid = require("uuid");
    var valu31 = uuid.v4();
    this.props.history.push("/info/"+valu31+"-"+providerId);
}
handleChange = date => this.setState({ date })


  render() {
    let gmailuser = localStorage.getItem("gmailuser");
    return (
      <>
        <Sidebar
          {...this.props}
          routes={routes}
          logo={{
            innerLink: "/admin/index",
            imgSrc: require("assets/img/logo.svg"),
            imgAlt: "..."
          }}
        />
        <div className="main-content d-flex align-items-top" ref="mainContent">
         <AdminNavbar1
          {...this.props}
          brandText={this.getBrandText(this.props.location.pathname)}
        />
        {/* Page content */}
        
        <div className="app-content">
        <Container className="" fluid>

        <div className="book-dr-info">
          <div className="book-dr-block">
            <span class="avatar avatar-sm rounded-circle rgster-avtar dr-avatar">
            <img alt="..." src={require('./../../assets/img/doctors/dr-prfle.jpg')} />
            </span>
            <div className="bool-dr-dtls">
              <h2>{this.state.Firstname} {this.state.Lastname}, {this.state.taxonomy_code}</h2>
              {/* <span>000-000-0000</span> */}
              <p>{this.state.adate} - {this.state.time} </p>
              <address>{this.state.address1}, {this.state.city}, {this.state.state}  {this.state.zip}</address>
            </div>
            </div>

            <div className="chng-date-block">
                <a onClick={() => this.goAppointmentChangeDate(this.state.provider_id)}>Change date</a>
            </div>
            
            </div>

            <div className="form-wrap-block" id="new">
              <form  onSubmit={this.onSubmit} className="" id="" action="">
              <Row className="align-items-top">
                        <Col xl="12">
                          <div className="block-title">
                            <h1>Patient</h1>
                          <h4 className="sub-title">New patient?</h4>

                          </div>
                          <div className="f-wrapper">
                            
                          <div className="form-group">
                            <input
 type="radio" 
                            id="test1" 
                            name="check1"
                            checked={this.state.yeschecked}
                            onChange={(e) => {
                              this.handleCheck({
                                target: {
                                  name: e.target.name,
                                  value: e.target.checked,
                                },
                              });
                            }}

                             />
                            <label for="test1">Yes</label>
                          </div>
                          
                          <div className="form-group">
                            <input
 type="radio" 
                            id="test2" 
                            name="check"
                            checked={this.state.nochecked}
                            onChange={(e) => {
                              this.handleCheck({
                                target: {
                                  name: e.target.name,
                                  value: e.target.checked,
                                },
                              });
                            }}
                             />
                            <label for="test2">No</label>
                          </div>
                          </div>
 
 
                        </Col>      
                      </Row>
                      <div className="form-details-block">
                        <Row className="align-items-top">
                          <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                              <div className="form-group">
                                <label>First Name, Last Name</label>
                                <input
required 
                                type="text" 
                                class="form-control" 
                                placeholder="First name"
                                id="firstname" 
                                
                                value={this.state.firstname}
                                onChange={evt =>
                                this.setState({
                                firstname: evt.target.value
                                })}
                      />
                              </div>
                              </div>
                          </Col>
                          
                          <Col xl="4" md="4" sm="12">
                          <div className="form-group-wrap">
                              <div className="form-group">
                                <label>Phone</label>
                                <input
required type="text" 
                                id="phone" 
                                
                                value={this.state.phone}
                                onChange={evt =>
                                this.setState({
                                  phone: evt.target.value
                                })}
                                class="form-control" 
                                placeholder="000-000-0000" />
                              </div>
                              </div>
                          </Col>
                          <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                              <div className="form-group">
                              <label>Date</label>
       

        <input type="text" id="date" readonly="true"                               
                                value={this.state.adate +' '+this.state.time}
                                
                                class="form-control" 
                                 />
                              </div>
                              </div>
                          </Col>

                          <Col xl="4" md="4" sm="12">
                          <div className="form-group-wrap">
                              <div className="form-group">
                                <label>Gender</label>
                                <input
required type="text"
                                class="form-control"
                                id="gender" 
                                
                                value={this.state.gender}
                                onChange={evt =>
                                this.setState({
                                  gender: evt.target.value
                                })}
                                placeholder="Male" />
                              </div>
                              </div>
                          </Col>
                          <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                              <div className="form-group">
                                <label>Email</label>
                                <input
required type="text" 
                                class="form-control" 
                                id="email" 
                                
                                value={gmailuser}
                                onChange={evt =>
                                this.setState({
                                  email: evt.target.value
                                })}
                                placeholder="" />
                              </div>
                              </div>
                          </Col>
                        </Row>
                        <Row className="align-items-top">
                            <div className="block-title bt">
                                <h1>Address</h1>
                              </div>
                              <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                              <div className="form-group">
                                <label>Address </label>
                                <input
required type="text" 
                                class="form-control" 
                                id="address" 
                                
                                value={this.state.address}
                                onChange={evt =>
                                this.setState({
                                  address: evt.target.value
                                })}
                                placeholder="123 Street" />
                              </div>
                              </div>
                          </Col>
                          {/* <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                              <div className="form-group">
                                <label>Address 2</label>
                                <input
required type="text" class="form-control" placeholder="" />
                              </div>
                              </div>
                          </Col> */}
                          <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                              <div className="form-group">
                                <label>City, State, Zip</label>
                                <input
required type="text" 
                                class="form-control"
                                id="zipcode" 
                                
                                value={this.state.zipcode}
                                onChange={evt =>
                                this.setState({
                                  zipcode: evt.target.value
                                })}
                                placeholder="City, State, 00000"
                                 />
                              </div>
                              </div>
                          </Col>
                        </Row>
                        <Row className="align-items-top">
                        <div className="block-title bt">
                                <h1>Insurance</h1>
                              </div>
                              <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                            
                            <select 
                               className="btn ins-drpdwn app-drpdwn dropdown-toggle" 
                               id="select"
                               defaultValue={this.state.selectinsuranceValue} 
                               onChange={this.handleinsuranceChange}
                               >
                                 <option value="">
                Select Insurance
              </option>
                              {this.state.insurancreason.map((insurancreason, idx) => {
            return (
              <option value={insurancreason.name} key={idx}>
                {insurancreason.name}
              </option>
            )
          })}
</select>
                           
                              </div>
                          </Col>
                        </Row>
                        <Row className="align-items-top">
                        <div className="block-title bt apbt">
                                <h1>Appointment</h1>
                              </div>
                              <Col xl="6" md="6" sm="12">
                            <div className="form-group-wrap">
                              <label>Visit reason</label>
                              <select 
                               className="btn ins-drpdwn app-drpdwn dropdown-toggle" 
                               id="myselect"
                               required
                               defaultValue={this.state.selectvisitValue} 
                               onChange={(e) => {
                                this.handlevisitChange({
                                  target: {
                                    name: e.target.name,
                                    value: e.target.value,
                                  },
                                });
                              }}

                               //onChange={this.handlevisitChange}
                               >
                                  <option value="">
                Select Visit
              </option>
                              {this.state.visitreason.map((visitreason, idx) => {
            return (
              <option name={visitreason.name} value={visitreason.reasonid} key={idx}>
                {visitreason.name}
              </option>
            )
          })}
</select>

                            {/* <Dropdown>
  <Dropdown.Toggle variant="success" id="dropdown-basic" className="btn ins-drpdwn app-drpdwn dropdown-toggle">
  Select
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Dropdown.Item >
    {this.state.visitreason.map((visitreason, idx) => {
            return (
              <li key={idx}>
                {visitreason.name}
              </li>
            )
          })}

    </Dropdown.Item>
  </Dropdown.Menu>
</Dropdown> */}




                            
                              </div>
                          </Col>
                        </Row>

                        <Row className="align-items-top">
                          <Col xl="12" md="12" sm="12"> 
                        <div className="form-group-wrap">
                        <textarea 
                        className="form-control txtarea" 
                        id="notes"
                        rows="3"
                        value={this.state.notes}
                                onChange={evt =>
                                this.setState({
                                  notes: evt.target.value
                                })}
                        ></textarea>
                        </div>
                        </Col>
                        <Col xl="12" md="12" sm="12">
                          <div className="book-now-btn">
                          <Button type="submit" id="btnClick"   className="bk-now-btn">Book Now</Button>
                          {this.state.Validationlogin}
            
                          </div>
                        </Col>

                        </Row>
                      </div>

              </form>
            </div>
         
        
        {/* <div className="app-booked-wrap dispaly">
          <div className="app-booked-appointment">
            <h1>Appointment booked!</h1>
            <h5>Today Dec 23, 2019 - 11:00AM</h5>
            <h6>Visit reason: <span>Cold</span></h6>
            <p>An confirmation email has been sent to name@email.com</p>
            <h2>25:00</h2>
            <div className="conform-app">
              <h3>Minutes remaining to confirm</h3>

              <div className="book-btns">
              <button type="submit" className="btn cnfrm-app">Confirm Appointment</button>
              <button type="submit" className="btn cancel-btn">Cancel</button>
              </div>
            </div>
          </div>
        </div> */}
<div className="modal" tabindex="-1" role="dialog" id = "bookloader">
  <div className="modal-dialog modal-dialog-centered" role="document">
    <div className="modal-content">
      <div className="modal-header temp-header">
      <div className="title-wrap">        <h5 className="modal-title">Booking</h5>
        </div>
        <div className="book-dr-info">
          <div className="book-dr-block">
            <span class="avatar avatar-sm rounded-circle rgster-avtar dr-avatar">
            <img alt="..." src={require('./../../assets/img/doctors/dr-prfle.jpg')} />
            </span>
            <div className="bool-dr-dtls">
              <h2>{this.state.Firstname} {this.state.Lastname}, {this.state.taxonomy_code}</h2>
              {/* <span>000-000-0000</span> */}
              <p>{this.state.adate} - {this.state.time}  AM</p>
              
            </div>
          </div>
        </div>
      </div>
      <div className="modal-body">
        
      <div className="appoint-redirect" id="spin-loader">
                <h1>We are booking your appointment.This could take a few seconds please be patient.</h1>
                <div className="spin-img">
                <img className="img-fluid" src={require('../../assets/img/icons/spinner.gif')}  alt="fireSpot"/>
                </div>
            </div>
        {/* <p>API response:- {this.state.Validationlogin}</p> */}
      </div>
     
    </div>
  </div>
</div>
        <div className="book-complete-wrap" id = "book">
        
          <div className="book-complete-inner">
          <h1>Appointment booked!</h1>
            <h5> {this.state.adate} - {this.state.time} </h5>
            <h6>Visit reason: <span>{this.state.selectvisitValue}</span></h6>
            <p>If you need to reschedule for any reason
Goto the <a href="">appointments</a> tab.</p>
            <h2>25:00</h2>
            <div className="conform-app">
              

              <div className="book-btns">
              <button type="submit" className="btn book-digital-checkin">Digital Check-in</button>
              <button type="submit" className="btn calender-btn">
                <img className="img-fluid" src={require('./../../assets/img/icons/calender.png')}  alt="fireSpot"/> 
                Add to calendar
                </button>
                <button type="submit" className="btn emailapp-btn">
                <img className="img-fluid" src={require('./../../assets/img/icons/email.png')}  alt="fireSpot"/> 
                Send a text directions
                </button>
                <button type="submit" className="btn another-email-btn">
                <img className="img-fluid" src={require('./../../assets/img/icons/text-directions.png')}  alt="fireSpot"/> 
                Send to another email
                </button>
                
              
              </div>
            </div>
          </div>

        </div>


        <div className="congratulations-wrap dispaly">
          <div className="congratulations-inner">
            <h1>Congratulations!</h1>
            <span>Your appointment is now booked. Please confirm below to keep your appointment.</span>
            <h5>Today Dec 23, 2019 - 11:00AM</h5>
            <h6>Visit reason: <span>Cold</span></h6>
            <p>An confirmation email has been sent to name@email.com</p>
            <h2>25:00</h2>
            <div className="conform-app">
              <h3>Minutes remaining to confirm</h3>

              <div className="book-btns">
                <button type="submit" className="btn cnfrm-app">Confirm Appointment</button>
              </div>
            </div>
            <div className="generated-details">
              <h2>Use your Email address and temporary password created to login.</h2>

              <p>Email:<span>useremail@email.com</span></p>
              <p>Temp password:<span>Yux456Z</span></p>
            </div>
          </div>
        </div>


</Container>
</div>
</div>
<Modal
          className="modal-dialog-centered"
          isOpen={this.state.exampleModal}
          toggle={() => this.toggleModal("exampleModal")}
        >
          <div id ="2" className="modal-header m-header">
            <div className="mdl-nm">
            <h5 className="modal-title" id="exampleModalLabel">
            Sign up 
            </h5>
            </div>
            

            <div className="signup-info">
          <span class="avatar avatar-sm rounded-circle rgster-avtar user-avatar">
            <img alt="..." src="/static/media/team-4-800x800.23007132.jpg" />

            </span>
            <div className="name-info">
              <h2>Dr. First Last, MD</h2>
              <p>Today Dec 23, 2019 - 11:00AM</p>
            </div>
            </div>
          </div>
           <div className="modal-footer">
          <div className="modal-sbmt-btn" id="msbmt">
          {/* <Link to="/admin/appointments" className="form-links">
          <button type="submit" className="btn msbmt-btn"  onClick={() => this.appointmentPopup()}>Sign Up</button>
                    </Link> */}
          
          </div>
          
            
            <div className="modal-cancel-btn" id="mcncl">
            <Button
              data-dismiss="modal"
              type="button"
              onClick={() => this.toggleModal("exampleModal")}
              className="cncl-btn"
            >
              Cancel
            </Button>
            </div>

            <div className="" id="spin-popup">
          <button type="submit" data-dismiss="modal"  type="button" className="btn cls-btn">Close</button>
          </div>
            
            
          </div>
          
        </Modal>


         </>
    );
  }
}

export default AppointmentForm;
