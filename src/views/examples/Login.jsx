
import React from "react";

// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";

import GoogleLogin from 'react-google-login';

class Login extends React.Component {
  
   constructor() {
        super();
        this.state = { isAuthenticated: false, user: null, token: ''};
    }

    logout = () => {
        this.setState({isAuthenticated: false, token: '', user: null})
    };
    googleResponse = (e) => {};
    onFailure = (error) => {
      alert(error);
    }


     googleResponse = (response) => {
        const tokenBlob = new Blob([JSON.stringify({access_token: response.accessToken}, null, 2)], {type : 'application/json'});
        const options = {
            method: 'POST',
            body: tokenBlob,
            mode: 'cors',
            cache: 'default'
        };
        console.log(tokenBlob);
        fetch('http://http://rd.mydom.com:3000/api/v1/auth/google', options).then(r => {
            const token = r.headers.get('x-auth-token');
            r.json().then(user => {
                if (token) {
                    this.setState({isAuthenticated: true, user, token})
                }
            });
        })
    };


  render() {
   const responseGoogle = (response) => {
      console.log(response);
    }
    let content = !!this.state.isAuthenticated ?
            (
                <div>
                    <p>Authenticated</p>
                    <div>
                        {this.state.user.email}
                    </div>
                    <div>
                        <button onClick={this.logout} className="button">
                            Log out
                        </button>
                    </div>
                </div>
            ) :
            (
              <GoogleLogin
        clientId="495161513893-3tk7gq2uhsoesv2gs757akal2nf05asb.apps.googleusercontent.com" //CLIENTID NOT CREATED YET
        buttonText="Google"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
      />
            );

    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
              
               
                

      <GoogleLogin
        clientId="495161513893-3tk7gq2uhsoesv2gs757akal2nf05asb.apps.googleusercontent.com" //CLIENTID NOT CREATED YET
        buttonText="Google"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
      />
            </Card>
        
        </Col>
      </>
    );
  }
}

export default Login;