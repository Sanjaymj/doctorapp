
import "assets/scss/main-colors.scss";
import "assets/vendor/@fortawesome/fontawesome-free/css/all.min.css";
import "assets/vendor/nucleo/css/nucleo.css";
import AdminLayout from "layouts/Admin.jsx";
import AuthLayout from "layouts/Auth.jsx";
import IframeLayout from "layouts/Iframe.jsx";
import React from "react";
import ReactDOM from "react-dom";
import { HashRouter , Redirect, Route, Switch } from "react-router-dom";
import Icons from "views/examples/Icons.jsx";
import AppointmentForm from "views/examples/AppointmentForm.jsx";


ReactDOM.render(
  <HashRouter >
    <Switch>
      <Route path="/admin" render={props => <AdminLayout {...props} />} />
      <Route path="/auth" render={props => <AuthLayout {...props} />} />
      <Route path="/iframe" render={props => <IframeLayout {...props} />} />
      <Route path="/info/:providerId" render={props => <Icons {...props} />} />
      <Route path="/appointments/:providerId/:appointmentId" render={props => <AppointmentForm {...props} />} />
      <Redirect from="/" to="/admin/doctor_list" />
    </Switch>
  </HashRouter >,
  document.getElementById("root")
);



